import React, { useState, useEffect } from 'react';
import {Link} from 'react-router-dom';
import axios from 'axios';

export default function Home () {

    const [data, setData] = useState([])

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api').then(response => {
            setData(response.data);
        });
    }, [])

    return (
        <div>
           <table className="table">
                <thead>
                    <tr>
                    <th scope="col">#</th>
                    <th>Action</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        data.map(row => {
                            return(
                                <tr key={row.id}>
                                <th scope="row">{row.id}</th>
                                <td> <Link to={"/edit/"+row.id}>Edit</Link> </td>
                                <td>{row.name}</td>
                                <td>{row.email}</td>
                                </tr>
                            )
                        })
                    }
                
                </tbody>
            </table>
        </div>
    )
}