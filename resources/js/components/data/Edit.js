import axios from "axios";
import React, {useEffect, useState} from 'react';

export default function Edit (props) {

    const [data, setData] = useState([])

    useEffect(() => {
        axios.get('http://127.0.0.1:8000/api/show/').then(response => {
           
            setData(response.data);
        });
    }, [])

    return (
        <div style={{padding: 100}}>
            <h1>Update Data</h1>
            <hr />
            <form>
                <div className = "mb-3">
                    <label htmlFor="name" className="form-label">Name</label>
                    <input 
                        type="text"
                        className="form-control"
                        id="name"
                        value={data.name}
                        placeholder="Enter Name"
                    />

                    <label htmlFor="email" className="form-label">Email</label>
                    <input 
                        type="email"
                        className="form-control"
                        id="email"
                        value={data.email}
                        placeholder="Enter Email"
                    />
                </div>
                <button type="button" className="btn btn-primary" >Save</button>
            </form>

        </div>
    )
}