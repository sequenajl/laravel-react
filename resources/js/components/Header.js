import React from 'react';
import {Route, Routes, Link } from 'react-router-dom';

import Home from './Home';
import About from './About';
import Contact from './Contact';
import Edit from './data/Edit';

export default function Header () {
    return (
        <div>
            <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                <Link className="navbar-brand" to="/">Navbar</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                <div className="navbar-nav">
               
                <Link className="nav-link" to="/">Home</Link>
                <Link className="nav-link" to="/about">About</Link>
                <Link className="nav-link" to="/contact">Contact</Link>
             
                </div>
            </div>
                </div>
            </nav>

         
                <Routes>
                    <Route path='/' element={<Home />} />
                    <Route path='/edit/:id' element={<Edit />} />
                    <Route path='/about' element={<About />} />
                    <Route path='/contact' element={<Contact />} />
                </Routes>
           
        </div>
    )
}